#!/bin/bash

# Usage
# If lando fails to setup start, just run `bash fail_helper.sh`

echo "composer requiring drush, devel..."
# "drupal/realistic_dummy_content" is not supported for Drupal 10
lando composer require "drush/drush:^11" "drupal/devel" --no-install
echo "composer installing packages..."
lando composer install
echo "creating necessary folder..."
mkdir -p ./web/sites/default/
chmod ug+w -R ./web/sites/default/
echo "installing site via drush"
lando drush si -y --account-pass=admin --site-name='Drupal, GitPod & Lando' --db-url=mysql://drupal10:drupal10@database/drupal10 standard
echo "enabling devel, devel_generate..."
lando drush en devel devel_generate -y
echo "generating users..."
lando drush devel-generate-users -y
echo "generating vocabularies..."
lando drush devel-generate-vocabs -y
echo "generating terms..."
lando drush devel-generate-terms --bundles=tags -y
echo "generating content..."
lando drush devel-generate-content -y
echo "generating menus..."
lando drush devel-generate-menus -y
echo "opening running website..."
gp preview $(gp url $(lando info --format=json | jq -r ".[0].urls[0]" | sed -e 's#http://localhost:\(\)#\1#'))
