#!/bin/bash

lando stop
lando rebuild -y
lando composer --no-install create-project drupal/recommended-project .temp
mv ./.temp/composer.json ./composer.json
mv ./.temp/composer.lock ./composer.lock
rmdir ./.temp
